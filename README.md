
Для развертывания и тестирования API:

1. Установите необходимые зависимости, выполнив команду `pip install -r req.txt`
2. Сохраните код в файл с именем `main.py`
3. Запустите сервер API с помощью команды `uvicorn main:app --reload`
4. Документация API доступна по адресу `http://localhost:8000/docs` или `http://localhost:8000/redoc`
5. Используйте инструменты, такие как cURL или Postman, для отправки запросов к конечным точкам API (например, `http://localhost:8000/signup`, `http://localhost:8000/login` и т. д.)