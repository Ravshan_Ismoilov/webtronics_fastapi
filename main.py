from fastapi import FastAPI, HTTPException, Depends
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from typing import List

from models import User, Post, Like, db

app = FastAPI()

oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token")


# Аутентификация
def authenticate_user(username: str, password: str):
    user = next((user for user in db["users"] if user["username"] == username), None)
    if user and user["password"] == password:
        return user

def get_current_user(token: str = Depends(oauth2_scheme)):
    user = next((user for user in db["users"] if user["username"] == token), None)
    if not user:
        raise HTTPException(status_code=401, detail="Неверный токен аутентификации")
    return user

# Маршруты
@app.post("/signup")
def signup(user: User):
    existing_user = next((u for u in db["users"] if u["username"] == user.username), None)
    if existing_user:
        raise HTTPException(status_code=400, detail="Имя пользователя уже существует")
    db["users"].append(user.dict())
    return {"message": "Пользователь успешно создан"}

@app.post("/login")
def login(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(status_code=401, detail="Неверное имя пользователя или пароль")
    return {"access_token": user["username"], "token_type": "bearer"}

@app.get("/posts", response_model=List[Post])
def get_posts():
    return db["posts"]

@app.post("/posts")
def create_post(post: Post, current_user: User = Depends(get_current_user)):
    post.author = current_user["username"]
    db["posts"].append(post.dict())
    return {"message": "Пост успешно создан"}

@app.put("/posts/{post_id}")
def edit_post(post_id: int, post: Post, current_user: User = Depends(get_current_user)):
    existing_post = next((p for p in db["posts"] if p["id"] == post_id), None)
    if not existing_post:
        raise HTTPException(status_code=404, detail="Пост не найден")
    if existing_post["author"] != current_user["username"]:
        raise HTTPException(status_code=403, detail="Вы не являетесь автором этого поста")
    existing_post.update(post.dict())
    return {"message": "Пост успешно обновлен"}

@app.delete("/posts/{post_id}")
def delete_post(post_id: int, current_user: User = Depends(get_current_user)):
    existing_post = next((p for p in db["posts"] if p["id"] == post_id), None)
    if not existing_post:
        raise HTTPException(status_code=404, detail="Пост не найден")
    if existing_post["author"] != current_user["username"]:
        raise HTTPException(status_code=403, detail="Вы не являетесь автором этого поста")
    db["posts"].remove(existing_post)
    return {"message": "Пост успешно удален"}

@app.post("/posts/{post_id}/like")
def like_post(post_id: int, current_user: User = Depends(get_current_user)):
    existing_post = next((p for p in db["posts"] if p["id"] == post_id), None)
    if not existing_post:
        raise HTTPException(status_code=404, detail="Пост не найден")
    if existing_post["author"] == current_user["username"]:
        raise HTTPException(status_code=403, detail="Вы не можете лайкнуть свой собственный пост")
    likes = db["likes"].get(post_id, [])
    if current_user["id"] in likes:
        raise HTTPException(status_code=400, detail="Вы уже лайкнули этот пост")
    likes.append(current_user["id"])
    db["likes"][post_id] = likes
    return {"message": "Пост успешно лайкнут"}

@app.post("/posts/{post_id}/dislike")
def dislike_post(post_id: int, current_user: User = Depends(get_current_user)):
    existing_post = next((p for p in db["posts"] if p["id"] == post_id), None)
    if not existing_post:
        raise HTTPException(status_code=404, detail="Пост не найден")
    likes = db["likes"].get(post_id, [])
    if current_user["id"] not in likes:
        raise HTTPException(status_code=400, detail="Вы не лайкали этот пост")
    likes.remove(current_user["id"])
    db["likes"][post_id] = likes
    return {"message": "Пост успешно дизлайкнут"}