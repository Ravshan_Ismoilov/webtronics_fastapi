from pydantic import BaseModel

# Внутренняя база данных для хранения постов и лайков
db = {
    "users": [],
    "posts": [],
    "likes": {}
}

# Модели
class User(BaseModel):
    username: str
    password: str

class Post(BaseModel):
    title: str
    content: str
    author: str

class Like(BaseModel):
    post_id: int
    user_id: int